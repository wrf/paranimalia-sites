# paranimalia-sites

data for article: [Francis, WR. and DE. Canfield (2020) Very few sites can reshape a phylogenetic tree. peerJ 8:e8865](https://peerj.com/articles/8865/)

The history of animal evolution, and the relative placement of extant animal phyla in this history is, in principle, testable from phylogenies derived from molecular sequence data. Though datasets have increased in size and quality in the past years, the contribution of individual genes (and ultimately amino acid sites) to the final phylogeny is unequal across genes. Here we demonstrate that removing a small fraction of sites strongly favoring one topology can produce a highly-supported tree of an alternate topology. We explore this approach using a dataset for animal phylogeny, and create a highly-supported tree with a monophyletic group of sponges and ctenophores, a topology not usually recovered. Because of the high sensitivity of such an analysis to gene selection, and because most gene sets are neither standardized nor representative of the entire genome, researchers should be more diligent about making intermediate analyses  available with their phylogenetic studies. To get a maximally informative dataset, possibly all genes need to be systematically examined across all species, potentially limiting analysis to species with sequenced genomes. From there, it could be determined whether any gene or gene sets introduce bias, and then deal with those biases appropriately.

All data and figures (excluding phylobayes chains) can be cloned with:

`git clone https://wrf@bitbucket.org/wrf/paranimalia-sites.git`

## [datasets](https://bitbucket.org/wrf/paranimalia-sites/src/master/datasets/) ##
The "weak" dataset in as `fasta` and `phylip-relaxed`: made from dataset 16 of [Whelan et al 2015](http://dx.doi.org/10.1073/pnas.1503453112) using the script `sitewise_get_strong_sites_2tree.py` along with the sitewise likelihood results from [Shen et al 2017](http://dx.doi.org/10.1038/s41559-017-0126).

`sitewise_get_strong_sites_2tree.py -w -a Whelan_D16_Opisthokonta_reduced.fasta -l RAxML_perSiteLLs.Whelan_D16_Opisthokonta_site_lk.tab -o whelan_d16_opisthokont_weak_sites.aln`

Other accessory scripts can be found [here](https://github.com/wrf/pdbcolor#raxml-site-wise-likelihood).

## [figures](https://bitbucket.org/wrf/paranimalia-sites/src/master/figures/) ##
* figure 1 : schematic of analysis
* figure 2 : overview of trees from the analysis
* figure 3 : full RAxML tree
* figure 4 : full phylobayes tree
* figure 5 : analyses of trimming/site-removal from various studies

## [phylobayes](https://bitbucket.org/wrf/paranimalia-sites/src/master/phylobayes/) ##
Trace and tree files from two chains, of 2000 iterations. Chains were continued from the previous run with `-x 1 2000`, meaning keeping every tree, for up to 2000 trees.

`mpirun -np 6 ~/phylobayes/pbmpi/data/pb_mpi -x 1 2000 d16_chain_01`

`mpirun -np 6 ~/phylobayes/pbmpi/data/pb_mpi -x 1 2000 d16_chain_02`

[Plot of traces](https://bitbucket.org/wrf/paranimalia-sites/src/master/phylobayes/d16_chain_01.trace.pdf) generated with [plot_phylobayes_traces.R](https://github.com/wrf/graphphylo/blob/master/plot_phylobayes_traces.R) R script.

## [raxml](https://bitbucket.org/wrf/paranimalia-sites/src/master/raxml/) ##
Normal RAxML run with 100 boostraps.

`raxmlHPC-PTHREADS-SSE3-8.2.11 -m PROTGAMMALGF -p 1776 -x 1776 -# 100 -f a -T 6 -s whelan_d16_opisthokont_weak_sites.phy -n whelan_d16o_weak`

and sitewise run of the three topologies on the "weak" dataset:

`raxmlHPC-PTHREADS-SSE3-8.2.11 -m PROTGAMMALGF -f G -T 6 -s whelan_d16_opisthokont_weak_sites.phy -n whelan_d16o_weak_sitewise -z whelan_d16o_weak_combined_trees`

Includes the [all-weak dataset](https://bitbucket.org/wrf/paranimalia-sites/src/master/raxml/all_weak/), where T3 sites are removed as well. This produces paraphyletic sponges and a clade of (ctenophores and (calcareous/homoscleromorphs)). This also has paraphyletic deuterostomes, as seen in the original dataset 16.

## [RF distance](https://bitbucket.org/wrf/paranimalia-sites/src/master/rf_distance/) ##
Pair-wise Robinson-Foulds distances were calculated using RAxML, with 7 trees, including the three base trees used to calculate the site-wise likelihoods (from Shen 2017), the two trees of the current study from RAxML and phylobayes, as well as the original trees from [Whelan et al 2015](https://doi.org/10.1073/pnas.1503453112) for datasets 16 and 10.

`raxmlHPC-PTHREADS-SSE3 -f r -z combined_trees_unrooted_for_RFD.tre -m PROTGAMMALGF -s Whelan_D16_Opisthokonta_reduced.fasta -n whelan_d16o_rf`

RF distance analysis of phylobayes chains was done similarly:

`raxmlHPC-PTHREADS-SSE3 -f r -z ../phylobayes/d16_chain_02.treelist -n d16_chain_02_1000t -m PROTGAMMALGF`

Only sequential trees (1 vs 2, 2 vs 3, etc.) are retained with the Python script [filter_rfd.py](https://github.com/wrf/graphphylo/blob/master/filter_rfd.py).

`~/git/graphphylo/filter_rfd.py RAxML_RF-Distances.d16_chain_02_1000t > d16_chain_02_1000t_rf_distances.txt`

The plot `d16_chain_02_1000t_rf_distances.pdf` is generated in R.

`Rscript ~/git/graphphylo/graph_filtered_rfd.R d16_chain_02_1000t_rf_distances.txt`

## [trimming stats](https://bitbucket.org/wrf/paranimalia-sites/src/master/trimming_stats/) ##
These were the data used to generate Figure 5. The stats for each human partition and the original protein were compiled using multiple scripts. The individual alignments of the partition with the original proteins were made following the [instructions here](https://github.com/wrf/heteropecilly#blast_to_align_pairs). These get collected for each dataset by the [instructions here](https://github.com/wrf/heteropecilly#alignment_pair_stats).

Some additional accessory scripts used are found [at my sequence processing repo](https://bitbucket.org/wrf/sequences).

An extra step had to be done for the Ryan 2013 dataset, as this did not have human orthologs in the set. For this, I used the [HMM-based method here](https://github.com/wrf/supermatrix/blob/master/add_taxa_to_align.py) to identify the closest human orthologs.



